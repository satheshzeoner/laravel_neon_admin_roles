<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

/* Services */

use App\Services\Users\UserspermissionService;

/* Repository */

use App\Repositories\Users\UserspermissionRepositories;
use Illuminate\Support\Facades\Session;

class UserpermissionController extends Controller
{

    private $UsersService;
    /**
     * @var UserspermissionService
     */
    private $UserspermissionService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->UserspermissionService      = new UserspermissionService();
        $this->UserspermissionRepositories = new UserspermissionRepositories();
    }

    /**
     * USER PERMISSION INDEX SCREEN.
     *
     */
    public function index()
    {
        $users['permission'] = $this->UserspermissionService->getUserPermissionList();
        $users['no']         = 0;
        return view('users/usersPermission/userPermissionDisplay')->with($users);
    }

    /**
     * USER PERMISSION ADD SCREEN
     *
     */
    public function userPermissionAdd()
    {
        $usersPermission['userpermissionmodule'] = collect($this->UserspermissionRepositories->getUserPermissionModuleList())->toArray();
        return view('users/usersPermission/userPermissionStore')->with($usersPermission);
    }

    /**
     * USER PERMISSION ADD STORE
     *
     */
    public function userPermissionStore(request $request)
    {
        $this->UserspermissionService->storeUserPermission($request);
        Session::flash('store', 'UserPermission Added Successfully');
        return redirect('usermanage/userpermission');
    }

    /**
     * USER PERMISSION EDIT SCREEN
     *
     */
    public function userPermissionEdit($masterId)
    {
        $usersPermission['permission']           = $this->UserspermissionService->getUserPermissionEditDetails($masterId);
        $usersPermission['permission_module']    = $usersPermission['permission']['module_id'];
        $usersPermission['userpermissionmodule'] = collect($this->UserspermissionRepositories->getUserPermissionModuleList())->toArray();
        return view('users/usersPermission/userPermissionUpdate')->with($usersPermission);
    }

    /**
     * USER PERMISSION UPDATE PROCESS
     *
     */
    public function userPermissionupdate(request $request)
    {
        $usersPermission['permission'] = $this->UserspermissionService->updateUserPermissionDetails($request);
        Session::flash('update', 'UserPermission Updated Successfully');
        return redirect('usermanage/userpermission');
    }

    /**
     * USER PERMISSION VIEW SCREEN
     *
     */
    public function userPermissionView($masterId)
    {
        $usersPermission['permission']           = $this->UserspermissionService->getUserPermissionEditDetails($masterId);
        $usersPermission['permission_module']    = $usersPermission['permission']['module_id'];
        $usersPermission['userpermissionmodule'] = collect($this->UserspermissionRepositories->getUserPermissionModuleList())->toArray();
        return view('users/usersPermission/userPermissionView')->with($usersPermission);
    }

    /**
     * USER PERMISSION DELETE PROCESS
     *
     */
    public function userPermissiondestroy(request $request)
    {
        $usersRole['userrole'] = $this->UserspermissionService->DestroyUserPermissionDetails($request);
        Session::flash('update', 'UserPermission Deleted Successfully');
        return redirect('usermanage/userpermission');
    }

    /*
     * USER PERMISSION DUPLICATE NAME CHECK
     *
     * */
    public function userPermissionDuplicateCheck(request $request)
    {
        return $this->UserspermissionService->getUserPermissionDuplicateStatus($request->all());
    }
}
