<?php

use Illuminate\Database\Seeder;
use App\Models\State\State;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function csv_to_array($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data   = array();
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {

                if (!$header)
                    $header = $row;
                else {
                    $data[] = array_combine($header, $row);
                }

            }
            fclose($handle);
        }

        return $data;
    }

    public function run()
    {
        $file  = base_path() . '/database/resources/state.csv';
        $datas = $this->csv_to_array($file, ',');

        foreach ($datas as $data) {
            $inc = State::where('state_id', $data['state_id'])->first();
            if ($inc == null) {
                State::create([
                    'state_name' => $data['state_name'],
                    'state_id'   => $data['state_id'],
                ]);
            }
        }

    }
}
