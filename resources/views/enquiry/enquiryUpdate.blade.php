@extends('layouts.app')

@section('content')

    <div class="row" style="margin-bottom: 30px">
        <div class="col-md-6"><h3>Update Enquiry</h3></div>
        <div class="col-md-6">
        </div>
    </div>

    <form role="form" id="form1" method="post" class="validate" action="{{ route('enquiryupdation') }}">
        @csrf

        <input type="hidden" name="master_id" value="{{ $enquiry->id }}">

        <div class="row">

            <table class="table">
                <tr>
                    <td class="col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Customer Name</label>

                            <input type="text" class="form-control" name="quotation[customer_name]"
                                   value="{{ $enquiry->customer_name }}"
                                   data-validate="required"
                                   data-message-required="Please fill Customer Name" placeholder="Customer Name"/>
                        </div>
                    </td>
                    <td class="col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Service</label>

                            <select name="quotation[service_id]" class="form-control service" data-validate="required"
                                    data-message-required="Select Service">
                                <option value="">Select Service</option>
                                @foreach(config('constants.service') as $key => $service)
                                    <option value="{{ $key }}"
                                            @if($key == $enquiry->service_id) selected @endif>{{ $service }}</option>
                                @endforeach
                            </select>
                        </div>
                    </td>
                    <td>
                        <div class="form-group other-service" @if($enquiry->service_id == 100) style="display: block"
                             @else style="display: none" @endif>
                            <label class="control-label">Other Service Name</label>

                            <input type="text" class="form-control" name="quotation[other_service_name]"
                                   value="{{ $enquiry->other_service_name }}"
                                   placeholder="Other Service Name"/>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Phone Number</label>

                            <input type="text" class="form-control" name="quotation[phone_number]"
                                   value="{{ $enquiry->phone_number }}"
                                   data-validate="required"
                                   data-message-required="Please fill Phone Number" placeholder="Phone Number"/>
                        </div>
                    </td>
                    <td class="col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Enquiry Type</label>

                            <select name="quotation[enquiry_type]" class="form-control status-select"
                                    data-validate="required"
                                    data-message-required="Select Status">
                                @foreach(config('constants.enquiry_status') as $key => $enquiry_status)
                                    <option value="{{ $key }}"
                                            @if($key == $enquiry->enquiry_type) selected @endif>{{ $enquiry_status }}</option>
                                @endforeach
                            </select>
                        </div>
                    </td>
                    <td class="col-lg-4">
                        <div class="form-group description" @if($enquiry->enquiry_type == 2) style="display: block"
                             @else style="display: none" @endif >
                            <label class="control-label">Description</label>

                            <input type="text" class="form-control"
                                   value=" @if($enquiry->enquiry_type == 2) {{ $enquiry->description }}
                                   @endif "
                                   name="quotation[description]" placeholder="Description"/>
                        </div>
                    </td>
                </tr>

            </table>


            <div class="form-group">
                <button type="submit" class="btn btn-success">Submit</button>
                <a href="{{ route('enquiry') }}" type="button" class="btn btn-blue">Cancel</a>
            </div>

    </form>


    <script>
        $(document).on('change', '.service', function (e) {
            let serviceId = $(this).val();
            if (serviceId == 100) {
                $('.other-service').show();
            } else {
                $('.other-service').hide();
            }
        });

        $(document).on('change', '.status-select', function (e) {
            let statusId = $(this).val();
            if (statusId == 2) {
                $('.description').val('');
                $('.description').show();
            } else {
                $('.description').hide();
            }
        });
    </script>

@endsection

