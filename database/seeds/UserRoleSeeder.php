<?php

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('users_role')->get()->count() == 0) {
            DB::table('users_role')->insert([
                'role_name'       => 'Administration',
                'role_permission' => '1',
                'created_at'      => now()
            ]);
        } else {
            echo "Table is not empty";
        }
    }
}
