<?php

namespace App\Services\Enquiry;

use App\Models\Enquiry\Enquiry;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Exception;


/* Repository */

use App\Repositories\Enquiry\EnquiryRepositories;

class EnquiryService
{

    public function __construct()
    {
        $this->EnquiryRepositories = new EnquiryRepositories();
    }

    # =============================================
    # =       ALL DETAILS                         =
    # =============================================

    public function getEnquiryList()
    {
        try {
            $details = collect($this->EnquiryRepositories->getEnquiryList())->toArray();
            return $details;
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =                 Add Details               =
    # =============================================

    public function storeEnquiry($request)
    {
        try {
            $quotation                         = $request->all();
            $quotation['quotation']['user_id'] = Auth::user()->id;
            return $this->EnquiryRepositories->storeEnquiryDetails($quotation['quotation']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    public function checkUniqueStatus($orderDetails)
    {

        $orderDetailsCheck['gender'] = $orderDetails['gender'];
        $orderDetailsCheck['dob']    = $orderDetails['dob'];
        $orderDetailsCheck['alt_id'] = $orderDetails['alt_id'];

        return Enquiry::where($orderDetailsCheck)
            ->orWhere('first_name', 'like', '%' . $orderDetails['first_name'] . '%')
            ->orWhere('last_name', 'like', '%' . $orderDetails['last_name'] . '%')
            ->exists();
    }

    # =============================================
    # =           Edit Details                    =
    # =============================================

    public function getEnquiryEditDetails($masterId)
    {
        try {
            return $this->EnquiryRepositories->getSpecificEnquiryDetails($masterId);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =                  Update                  =
    # =============================================

    public function UpdateEnquiryDetails(request $request)
    {
        try {
            $updateDetails = $request->all();
            $masterId      = $updateDetails['master_id'];
            return $this->EnquiryRepositories->updateEnquiry($masterId, $updateDetails['quotation']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =             Destroy                       =
    # =============================================

    public function DestroyEnquiryDetails(request $request)
    {
        $postDetails = $request->all();
        try {
            return $this->EnquiryRepositories->destroyEnquiry($postDetails['masterid']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =      Enquiry ORDER STATUS UPDATE          =
    # =============================================

    public function enquiryOrderStatus(request $request)
    {
        try {
            $updateDetails = $request->all();
            $masterId      = $updateDetails['master_id'];
            return $this->EnquiryRepositories->updateEnquiry($masterId, $updateDetails['quotation']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    public function doErrorFormat($responseResult)
    {
        Log::error($responseResult->getMessage());
        $result['status']  = false;
        $result['message'] = $responseResult->getMessage();
        return $result;
    }
}
