@extends('layouts.app')

@section('content')

    <div class="row" style="margin-bottom: 30px">
        <div class="col-md-6"><h3>Permission Update</h3></div>
        <div class="col-md-6">
        </div>
    </div>

    <form role="form" id="form1" method="post" class="validate form-horizontal form-groups-bordered"
          action="{{ route('userpermissionupdation') }}">
        @csrf

        <div class="form-group">
            <label class="control-label error-permission">Permission Name</label>
            <input type="hidden" name="master_id" value="{{ $permission['id']  }}"/>
            <input type="text" class="form-control permission_check" name="permission[permission_name]"
                   data-validate="required"
                   data-message-required="Please fill Permission name" placeholder="Permission Name"
                   value="{{ $permission['permission_name']  }}"/>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-1 col-sm-5">
                @foreach ($userpermissionmodule as $row)
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name=permission[module_id][]
                                   value="{{ $row['id'] }}" <?php echo (in_array($row['id'], $permission_module)) ? 'checked' : '' ?> >{{ $row['module_name'] }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success">Update</button>
            <a href="{{ route('userpermission') }}" type="button" class="btn btn-blue">Cancel</a>
        </div>

    </form>

    {{-- Declaration --}}
    <script>
        const except_id = {{ $permission['id']  }};
    </script>

    {{-- Custom Script --}}
    <script src={{ url("assets/lis/user/userPermission.js") }}></script>

@endsection

