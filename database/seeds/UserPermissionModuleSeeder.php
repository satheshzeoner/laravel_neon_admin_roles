<?php

use Illuminate\Database\Seeder;

class UserPermissionModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('users_permission_modules')->get()->count() == 0) {
            DB::table('users_permission_modules')->insert([
                'module_name' => 'User Management',
                'created_at'  => now()
            ]);
        } else {
            echo "Table is not empty";
        }
    }
}
