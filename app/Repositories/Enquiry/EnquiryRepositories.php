<?php

namespace App\Repositories\Enquiry;

/* Models */

use App\Models\Enquiry\Enquiry;
use Illuminate\Support\Facades\Log;

class EnquiryRepositories
{
    /* ===============================CREATE=============================== */

    /* Get Create Details */
    public function storeEnquiryDetails($Details)
    {
        $detailsResult = Enquiry::create($Details);
        Log::info('Enquirys Details has Inserted :' . $detailsResult->id);
        return $detailsResult->id;
    }

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* Get Enquiry Details */
    public function getEnquiryList()
    {
        return Enquiry::orderBy('id', 'DESC')->get();
    }

    /* Get Specific Enquiry Details */
    public function getSpecificEnquiryDetails($masterId)
    {
        return Enquiry::whereId($masterId)->first();
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* Get Specific Enquiry Details */
    public function updateEnquiry($masterId, $details)
    {
        return Enquiry::whereId($masterId)->update($details);
    }

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* Get Specific Enquiry Details */
    public function destroyEnquiry($masterId)
    {
        return Enquiry::whereId($masterId)->delete();
    }

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
