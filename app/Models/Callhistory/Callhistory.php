<?php

namespace App\Models\Callhistory;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Callhistory extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'call_history';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'enquiry_id',
        'status_id',
        'description',
        'status',
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = true;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    // const CREATED_AT = 'created';
    // const UPDATED_AT = 'updated';

//    public function userList()
//    {
//        return $this->hasOne(User::class, 'id', 'user_id');
//    }

}
