<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {

    /* USER MANGEMENT */

    Route::group(['prefix' => 'usermanage'], function () {
        /* User */
        Route::get('/user', 'User\UserController@index')->name('user');

        /* User ADD */
        Route::get('/useradd', 'User\UserController@userAdd')->name('useradd');
        Route::post('/userstore', 'User\UserController@userStore')->name('userstore');

        /* User Role EDIT */
        Route::get('/userupdate/{id}', 'User\UserController@userEdit')->name('userupdate');
        Route::post('/userupdation', 'User\UserController@userUpdate')->name('userupdation');

        /* User Role VIEW */
        Route::get('/userview/{id}', 'User\UserController@userView')->name('userview');

        /* user DELETE */
        Route::post('/userdestroy', 'User\UserController@userDestroy')->name('userdestroy');

        /* ----------------------------------------------------------------------------------------------------------------  */

        /* User Role */
        Route::get('/userrole', 'User\UserroleController@index')->name('userrole');

        /* User Role ADD */
        Route::get('/userroleadd', 'User\UserroleController@userRoleAdd')->name('userroleadd');
        Route::post('/userrolestore', 'User\UserroleController@userRoleStore')->name('userrolestore');

        /* User Role EDIT */
        Route::get('/userroleupdate/{id}', 'User\UserroleController@userRoleEdit')->name('userroleupdate');
        Route::post('/userroleupdation', 'User\UserroleController@userRoleupdate')->name('userroleupdation');

        /* User Role Duplicate Check  AJAX */
        Route::post('/userrolecheck', 'User\UserroleController@userRoleDuplicateCheck')->name('userrolecheck');

        /* User Role VIEW */
        Route::get('/userroleview/{id}', 'User\UserroleController@userRoleView')->name('userroleview');

        /* user Role DELETE */
        Route::post('/userroledestroy', 'User\UserroleController@userRoledestroy')->name('userroledestroy');

        /* ----------------------------------------------------------------------------------------------------------------  */

        /* User Permission */
        Route::get('/userpermission', 'User\UserpermissionController@index')->name('userpermission');

        /* User Permission ADD */
        Route::get('/userpermissionadd', 'User\UserpermissionController@userPermissionAdd')->name('userpermissionadd');
        Route::post('/userpermissionstore', 'User\UserpermissionController@userPermissionStore')->name('userpermissionstore');

        /* User Permission EDIT */
        Route::get('/userpermissionupdate/{id}', 'User\UserpermissionController@userPermissionEdit')->name('userpermissionupdate');
        Route::post('/userpermissionupdation', 'User\UserpermissionController@userPermissionupdate')->name('userpermissionupdation');

        /* User Permission Duplicate Check  AJAX */
        Route::post('/userpermissioncheck', 'User\UserpermissionController@userPermissionDuplicateCheck')->name('userpermissioncheck');

        /* User Permission VIEW */
        Route::get('/userpermissionview/{id}', 'User\UserpermissionController@userPermissionView')->name('userpermissionview');

        /* user Permission DELETE */
        Route::post('/userpermissiondestroy', 'User\UserpermissionController@userPermissiondestroy')->name('userpermissiondestroy');

        /* ----------------------------------------------------------------------------------------------------------------  */
    });

    /* ----------------------------------------------------------------------------------------------------------------  */

    Route::group(['prefix' => 'enquiry'], function () {

        /* Enquiry View Page */
        Route::get('/enquiry', 'Enquiry\EnquiryController@index')->name('enquiry');

        /* Enquiry Add Page */
        Route::get('/enquiryadd', 'Enquiry\EnquiryController@enquiryAdd')->name('enquiryadd');

        /* Enquiry Store Page */
        Route::post('/enquirystore', 'Enquiry\EnquiryController@enquiryStore')->name('enquirystore');

        /* Enquiry Edit Page */
        Route::get('/enquiryEdit/{id}', 'Enquiry\EnquiryController@enquiryEdit')->name('enquiryEdit');
        Route::post('/enquiryupdation', 'Enquiry\EnquiryController@enquiryUpdate')->name('enquiryupdation');

        /* Enquiry DELETE */
        Route::post('/enquirydestroy', 'Enquiry\EnquiryController@enquiryDestroy')->name('enquirydestroy');

    });

    /* ----------------------------------------------------------------------------------------------------------------  */

    Route::group(['prefix' => 'callhistory'], function () {

        /* Callhistory View Page */
        Route::get('/callhistory', 'Callhistory\CallhistoryController@index')->name('callhistory');

        /* Callhistory Store Page */
        Route::post('/callhistoryupdate', 'Callhistory\CallhistoryController@callhistoryupdate')->name('callhistoryupdate');

    });

    Route::group(['prefix' => 'order'], function () {

        /* Callhistory View Page */
        Route::get('/order', 'Order\OrderController@index')->name('order');

        /* Callhistory Store Page */
        Route::post('/orderupdate', 'Order\OrderController@orderUpdate')->name('orderUpdate');

    });

    Route::group(['prefix' => 'report'], function () {

        /* Enquiry Report Page */
        Route::get('/enquiry-report', 'Report\ReportController@enquiryReport')->name('enquiry-report');

        /* Enquiry Report Search Result */
        Route::post('/enquiry-report-result', 'Report\ReportController@enquiryReportResult')->name('enquiry-report-result');

        /* Call Report Page */
        Route::get('/call-report', 'Report\ReportController@callReport')->name('call-report');

        /* Call Report Search Result */
        Route::post('/call-report-result', 'Report\ReportController@callReportResult')->name('call-report-result');

        /* Order Report Page */
        Route::get('/order-report', 'Report\ReportController@orderReport')->name('order-report');

        /* Order Report Search Result */
        Route::post('/order-report-result', 'Report\ReportController@orderReportResult')->name('order-report-result');


    });

});

