<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/* Services */

use App\Services\Report\ReportService;
use Illuminate\Support\Facades\Session;

class ReportController extends Controller
{

    /**
     * @var ReportService
     */
    private $ReportService;

    public function __construct()
    {
        $this->ReportService = new ReportService();
    }

    /**
     * ENQUIRY REPORT SCREEN.
     *
     */
    public function enquiryReport()
    {
        return view('report.enquiryReport.enquiryReportDisplay');
    }

    /**
     * ENQUIRY REPORT-RESULT SCREEN.
     *
     */
    public function enquiryReportResult(request $request)
    {
        $enquiry['enquiry'] = $this->ReportService->getEnquiryList($request->all());
        $enquiry['no']      = 0;
        return view('report.enquiryReport.enquiryReportAjax')->with($enquiry);
    }

    /**
     * CALL REPORT SCREEN.
     *
     */
    public function callReport()
    {
        return view('report.callReport.callReportDisplay');
    }

    /**
     * CALL REPORT-RESULT SCREEN.
     *
     */
    public function callReportResult(request $request)
    {
        $call['callreport'] = $this->ReportService->getCallList($request->all());
        $call['no']         = 0;
        return view('report.callReport.callReportAjax')->with($call);
    }

    /**
     * ORDER REPORT SCREEN.
     *
     */
    public function orderReport()
    {
        return view('report.orderReport.orderReportDisplay');
    }

    /**
     * ORDER REPORT-RESULT SCREEN.
     *
     */
    public function orderReportResult(request $request)
    {
        $order['orderreport'] = $this->ReportService->getOrderList($request->all());
        $order['no']          = 0;
        return view('report.orderReport.orderReportAjax')->with($order);
    }


}
