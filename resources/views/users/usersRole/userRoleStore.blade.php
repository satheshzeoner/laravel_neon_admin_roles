@extends('layouts.app')

@section('content')

    <div class="row" style="margin-bottom: 30px">
        <div class="col-md-6"><h3>New Role</h3></div>
        <div class="col-md-6">
        </div>
    </div>

    <form role="form" id="form1" method="post" class="validate" action="{{ route('userrolestore') }}">
        @csrf

        <div class="form-group">
            <label class="control-label error-role">Role Name</label>

            <input type="text" class="form-control role_check" name="role[role_name]" data-validate="required"
                   data-message-required="Please fill Role name" placeholder="Role Name"/>
        </div>

        <div class="form-group">
            <label class="control-label">Role Permission</label>
            <select class="select2" name="role[role_permission][]" required multiple>
                @foreach ($userpermission as $row)
                    <option value="{{ $row->id }}">{{ $row->permission_name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success">Save</button>
            <a href="{{ route('userrole') }}" type="button" class="btn btn-blue">Cancel</a>
        </div>

    </form>

    {{-- Declaration --}}
    <script>
        const except_id = '';
    </script>

    {{-- Custom Script --}}
    <script src={{ url("assets/lis/user/userRole.js") }}></script>

@endsection

