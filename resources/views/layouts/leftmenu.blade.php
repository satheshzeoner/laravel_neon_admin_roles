<?php $userPermissionList = session()->get('user_permission'); ?>

<ul id="main-menu" class="main-menu">
    <li class="{{ request()->is('home') ? 'active' : '' }}">
        <a href="{{ route('home') }}">
            <i class="entypo-gauge"></i>
            <span class="title">Dashboard</span>
        </a>
    </li>

    @if(in_array(config('constants.user_nav_name.Enquiry'), $userPermissionList))
        <li class="{{ request()->is('enquiry/*') ? 'opened active has-sub root-level' : 'has-sub' }}">
            <a href="#">
                <i class="entypo-window"></i>
                <span class="title">Enquiry</span>
            </a>
            <ul>
                <li class="{{ request()->is('enquiry/enquiryadd') ? 'active' : '' }}">
                    <a href="{{ route('enquiryadd') }}">
                        <i class="entypo-list-add"></i>
                        <span class="title">Add Enquiry</span>
                    </a>
                </li>
                <li class="{{ request()->is('enquiry/enquiry') ? 'active' : '' }}">
                    <a href="{{ route('enquiry') }}">
                        <i class="entypo-list"></i>
                        <span class="title">Manage Enquiry</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif

    @if(in_array(config('constants.user_nav_name.CallHistory'), $userPermissionList))
        <li class="{{ request()->is('callhistory/*') ? 'active' : '' }}">
            <a href="{{ route('callhistory') }}">
                <i class="entypo-phone"></i>
                <span class="title">Call History</span>
            </a>
        </li>
    @endif

    @if(in_array(config('constants.user_nav_name.Order'), $userPermissionList))
        <li class="{{ request()->is('order/*') ? 'active' : '' }}">
            <a href="{{ route('order') }}">
                <i class="entypo-basket"></i>
                <span class="title">Orders</span>
            </a>
        </li>
    @endif

    @if(in_array(config('constants.user_nav_name.Report'), $userPermissionList))
        <li class="{{ request()->is('report/*') ? 'opened active has-sub root-level' : 'has-sub' }}">
            <a href="#">
                <i class="entypo-newspaper"></i>
                <span class="title">Report</span>
            </a>
            <ul>
                <li class="{{ request()->is('report/enquiry-report') ? 'active' : '' }}">
                    <a href="{{ route('enquiry-report') }}">
                        <i class="entypo-window"></i>
                        <span class="title">Enquiry Report</span>
                    </a>
                </li>
                <li class="{{ request()->is('report/call-report') ? 'active' : '' }}">
                    <a href="{{ route('call-report') }}">
                        <i class="entypo-database"></i>
                        <span class="title">Call Report</span>
                    </a>
                </li>
                <li class="{{ request()->is('report/order-report') ? 'active' : '' }}">
                    <a href="{{ route('order-report') }}">
                        <i class="entypo-basket"></i>
                        <span class="title">Order Report</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif


    @if(in_array(config('constants.user_nav_name.User_Management'), $userPermissionList))
        <li class="{{ request()->is('usermanage/*') ? 'opened active has-sub root-level' : 'has-sub' }}">
            <a href="#">
                <i class="entypo-window"></i>
                <span class="title">Administration</span>
            </a>
            <ul>
                <li class="{{ request()->is('usermanage/userpermission') ? 'active' : '' }}">
                    <a href="{{ route('userpermission') }}">
                        <i class="entypo-key"></i>
                        <span class="title">Permissions</span>
                    </a>
                </li>
                <li class="{{ request()->is('usermanage/userrole') ? 'active' : '' }}">
                    <a href="{{ route('userrole') }}">
                        <i class="entypo-star"></i>
                        <span class="title">Roles</span>
                    </a>
                </li>
                <li class="{{ request()->is('usermanage/user') ? 'active' : '' }}">
                    <a href="{{ route('user') }}">
                        <i class="entypo-users"></i>
                        <span class="title">Users</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif


</ul>
