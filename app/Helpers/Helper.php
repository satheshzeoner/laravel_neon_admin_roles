<?php

namespace App\Helpers;


use App\Models\State\State;

class Helper
{

    public static function getStates()
    {
        return State::get()->all();
    }

}
