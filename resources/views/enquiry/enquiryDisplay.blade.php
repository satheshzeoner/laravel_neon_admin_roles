@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-6"><h3>Enquiry</h3></div>
        <div class="col-md-6 bs-example">
            <a href="{{route('enquiryadd')}}" type="button" class="btn btn-success" style="float: right">Add Enquiry
            </a>
        </div>
    </div>

    <br/>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            var $table4 = jQuery("#table-4");

            $table4.DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });
        });
    </script>

    <table class="table table-bordered datatable" id="table-4">
        <thead>
        <tr>
            <th>S.no</th>
            <th>Customer Name</th>
            <th>Phone Number</th>
            <th>Service</th>
            <th>Enquiry Type</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($enquiry as $row)
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $row['customer_name'] }}</td>
                <td>{{ $row['phone_number'] }}</td>
                <td>
                    @if($row['service_id'] == 100)
                        {{ config('constants.service')[$row['service_id']].'- ['.$row['other_service_name'].']' }}
                    @else
                        {{ config('constants.service')[$row['service_id']] }}
                    @endif
                </td>
                <td>
                    <div class="label @if($row['enquiry_type'] == 1) label-info @else label-primary  @endif ">{{ config('constants.enquiry_status')[$row['enquiry_type']] }}</div>
                </td>
                <td class="text-center">

                    <a href="{{ url('enquiry/enquiryEdit/'.$row['id']) }}" class="btn btn-orange">
                        <i class="entypo-pencil"></i>
                    </a>

                    <button type="button" class="btn btn-danger" onclick="deleteaction({{ $row['id'] }})">
                        <i class="entypo-cancel"></i>
                    </button>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    {{-- Script For Delete --}}
    <script>
        function deleteaction(masterId) {
            jQuery('#modal-patienttype').modal('show');
            $('.deleteid').val(masterId);
        }

        function orderstatusaction(masterId) {
            jQuery('#modal-orderstatus').modal('show');
            $('.statusid').val(masterId);
        }

        function bulkUpload() {
            jQuery('#modal-patient-bulk-upload').modal('show');
        }
    </script>

    {{-- DELETE MODEL--}}

    <!-- Modal 1 (Basic)-->
    <div class="modal fade" id="modal-patienttype">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('enquirydestroy') }}" method="post">
                    @csrf

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Enquiry Delete</h4>
                    </div>

                    <div class="modal-body">
                        Are You Sure Want To Delete
                        <input type="hidden" name="masterid" class="deleteid" value="">
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    {{-- ENQUIRY ORDER STATUS MODEL--}}

    <!-- Modal 1 (Basic)-->
    <div class="modal fade" id="modal-orderstatus">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="#" method="post">
                    @csrf

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Enquiry Order Status</h4>
                    </div>

                    <div class="modal-body">
                        Are You Sure Want To Convert to Order
                        <input type="hidden" name="masterid" class="statusid" value="">
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Yes</button>
                    </div>
                </form>

            </div>
        </div>
    </div>


@endsection
