<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/* Services */

use App\Services\Users\ClienttypeService;
use Illuminate\Support\Facades\Session;

class ClienttypeController extends Controller
{
    /**
     * @var ClienttypeService
     */
    private $ClienttypeService;

    /**
     * Client Type Constructor
     *
     */
    public function __construct()
    {
        $this->ClienttypeService = new ClienttypeService();
    }

    /**
     * Show the Client Type Main Screen.
     *
     */
    public function index()
    {
        $users['clientType'] = $this->ClienttypeService->getClientTypeList();
        $users['no']         = 0;
        return view('users/clientType/clientTypeDisplay')->with($users);
    }

    /**
     * CLIENT TYPE ADD SCREEN.
     *
     */
    public function clientTypeAdd()
    {
        return view('users/clientType/clientTypeStore');
    }

    /**
     * CLIENT TYPE ADD STORE
     *
     */
    public function clietTypeStore(request $request)
    {
        $this->ClienttypeService->storeClientType($request);
        Session::flash('store', 'Client Type Added Successfully');
        return redirect('usermanage/clienttype');
    }

    /**
     * CLIENT TYPE EDIT SCREEN
     *
     */
    public function clientTypeEdit($masterId)
    {
        $clientType['clientType'] = $this->ClienttypeService->getClientTypeEditDetails($masterId);
        return view('users/clientType/clientTypeUpdate')->with($clientType);
    }

    /**
     * CLIENT TYPE UPDATE PROCESS
     *
     */
    public function clientTypeUpdate(request $request)
    {
        $usersRole['users'] = $this->ClienttypeService->UpdateClientTypeDetails($request);
        Session::flash('update', 'Client Type Updated Successfully');
        return redirect('usermanage/clienttype');
    }

    /**
     * CLIENT TYPE VIEW SCREEN
     *
     */
    public function clientTypeView($masterId)
    {
        $clientType['clientType'] = $this->ClienttypeService->getClientTypeEditDetails($masterId);
        return view('users/clientType/clientTypeView')->with($clientType);
    }

    /**
     * CLIENT TYPE DELETE PROCESS
     *
     */
    public function clientTypeDestroy(request $request)
    {
        $users['user'] = $this->ClienttypeService->DestroyClientTypeDetails($request);
        Session::flash('destroy', 'Client Type Deleted Successfully');
        return redirect('usermanage/clienttype');
    }
}
