@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-6"><h3>Order</h3></div>
        <div class="col-md-6 bs-example">

        </div>
    </div>

    <br/>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            var $table4 = jQuery("#table-4");

            $table4.DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });
        });
    </script>

    <table class="table table-bordered datatable" id="table-4">
        <thead>
        <tr>
            <th>S.no</th>
            <th>Attender</th>
            <th>Date</th>
            <th>Cust Name</th>
            <th>Phone</th>
            <th>Service</th>
            <th class="text-center">Enquiry-Type</th>
            <th class="text-center">Tracking</th>
            <th class="text-center">Order Description</th>
            <th class="text-center">Update</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($order as $row)
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $row['user_list']['fname'].'-'.$row['user_list']['lname'] }}</td>
                <td>{{ date('d-m-Y H:i A',strtotime($row['created_at'])) }}</td>
                <td>{{ $row['customer_name'] }}</td>
                <td>{{ $row['phone_number'] }}</td>
                <td>
                    @if($row['service_id'] == 100)
                        {{ config('constants.service')[$row['service_id']].'- ['.$row['other_service_name'].']' }}
                    @else
                        {{ config('constants.service')[$row['service_id']] }}
                    @endif
                </td>
                <td class="text-center">
                    <div class="label @if($row['enquiry_type'] == 1) label-info @else label-primary  @endif ">{{ config('constants.enquiry_status')[$row['enquiry_type']] }}</div>
                </td>

                <td class="text-center">
                    <select class="form-control status-select-{{ $row['id'] }}"
                            data-validate="required"
                            data-message-required="Select Status">
                        @foreach(config('constants.tracking_status') as $key => $value)
                            <option value="{{ $key }}"
                                    @if(!empty($row['orderlist']))
                                    @if($row['orderlist']['order_status'] == $key)
                                    selected
                                    @endif
                                    @endif
                            >{{ $value }}</option>
                    @endforeach
                </td>
                <td class="text-center">
                    @if(!empty($row['orderlist']))
                        <input type="text" class="form-control description-{{ $row['id'] }}"
                               value="{{ $row['orderlist']['order_description'] }}">
                    @else
                        <input type="text" class="form-control description-{{ $row['id'] }}" value="">
                    @endif
                </td>

                <td class="text-center">

                    <button type="button" class="btn btn-success" onclick="orderstatusaction({{ $row['id'] }})">
                        <i class="entypo-upload"></i>
                    </button>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    {{-- Script For Delete --}}
    <script>
        function orderstatusaction(masterId) {
            jQuery('#modal-orderstatus').modal('show');

            var statusId = $('.status-select-' + masterId).val();
            var description = $('.description-' + masterId).val();


            $('.status_id').val(statusId);
            $('.description').val(description);
            $('.master_id').val(masterId);
        }
    </script>


    {{-- ENQUIRY ORDER STATUS MODEL--}}

    <!-- Modal 1 (Basic)-->
    <div class="modal fade" id="modal-orderstatus">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('orderUpdate') }}" method="post">
                    @csrf

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Order Processing State</h4>
                    </div>

                    <div class="modal-body">
                        Are You Sure Want Update Status
                        <input type="hidden" name="order[order_status]" class="form-control status_id" value=""/>
                        <input type="hidden" name="order[order_description]" class="form-control description" value=""/>
                        <input type="hidden" name="order[enquiry_id]" class="form-control master_id" value=""/>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Yes</button>
                    </div>
                </form>

            </div>
        </div>
    </div>


@endsection
