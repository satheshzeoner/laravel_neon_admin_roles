@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-6"><h3>Call History Report</h3></div>
        <div class="col-md-6 bs-example">

        </div>
    </div>

    <br/>

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-info" data-collapsed="0">

                <!-- panel head -->
                <div class="panel-heading">
                    <div class="panel-title">Search Filter</div>

                    <div class="panel-options">

                    </div>
                </div>

                <!-- panel body -->
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Service</label>

                                <select class="form-control service-type">
                                    <option value="">Select Service</option>
                                    @foreach(config('constants.service') as $key => $service)
                                        <option value="{{ $key }}">{{ $service }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Enquiry-Type</label>

                                <select class="form-control enquiry-type">
                                    <option value="">Select Enquiry-Type</option>
                                    @foreach(config('constants.enquiry_status') as $key => $enquiry_status)
                                        <option value="{{ $key }}">{{ $enquiry_status }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Status</label>

                                <select class="form-control call-status">
                                    <option value="">Select Status</option>
                                    @foreach(config('constants.quotation_status') as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Customer-Name/Phone-Number</label>

                                <input type="text" class="form-control text-search"
                                       placeholder="Customer-Name/Phone-Number"/>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">From-Date</label>

                                <input type="text" class="form-control datepicker from-date"
                                       name="quotation[phone_number]"
                                       data-format="d-m-yyyy"
                                       placeholder="From Date"/>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">To-Date</label>

                                <input type="text" class="form-control datepicker to-date"
                                       name="quotation[phone_number]"
                                       data-format="d-m-yyyy"
                                       placeholder="To Date"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <button class="btn btn-success btn-block search-ajax-result">Search</button>
                            </div>
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>

    <div id="call-report-result">

        <table class="table table-bordered datatable" id="table-4">
            <thead>
            <tr>
                <th>S.no</th>
                <th>Attender</th>
                <th>Date</th>
                <th>Cust Name</th>
                <th>Phone</th>
                <th>Service</th>
                <th class="text-center">Enquiry-Type</th>
                <th class="text-center">Status</th>
                <th class="text-center">Description</th>
                <th class="text-center">Update</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>



    <script>
        $(document).on('click', '.search-ajax-result', function () {

            var serviceType = $('.service-type').val();
            var enquiryType = $('.enquiry-type').val();
            var callStatus = $('.call-status').val();
            var textSearch = $('.text-search').val();
            var fromDate = $('.from-date').val();
            var toDate = $('.to-date').val();

            $.ajax({
                type: "POST",
                url: base_url + "/report/call-report-result",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    "service_type": serviceType,
                    "enquiry_type": enquiryType,
                    "call_status": callStatus,
                    "text_search": textSearch,
                    "from_date": fromDate,
                    "to_date": toDate,
                },
                cache: true,
                async: false,
                success: function (data) {
                    $('#call-report-result').empty();
                    $('#call-report-result').append(data);
                }
            });

        });
    </script>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            var $table4 = jQuery("#table-4");

            $table4.DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });
        });
    </script>


@endsection
