<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('users')->get()->count() == 0) {
            DB::table('users')->insert([
                'fname'      => 'admin',
                'lname'      => 'admin',
                'email'      => 'admin@admin.com',
                'user_name'  => 'admin@admin.com',
                'phone'      => '9898989898',
                'address'    => 'admin',
                'role_id'    => '1',
                'password'   => bcrypt('12345'),
                'created_at' => now()
            ]);
        } else {
            echo "Table is not empty";
        }
    }
}
