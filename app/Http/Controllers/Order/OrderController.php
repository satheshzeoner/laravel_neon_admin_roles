<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/* Services */

use App\Services\Order\OrderService;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{

    /**
     * @var OrderService
     */
    private $OrderService;

    public function __construct()
    {
        $this->OrderService = new OrderService();
    }

    /**
     * CALL HISTORY INDEX SCREEN.
     *
     */
    public function index()
    {
        $order['order'] = $this->OrderService->getOrderList();
        $order['no']    = 0;
//        dd($order);
        return view('order.orderDisplay')->with($order);
    }

    /**
     * CALL HISTORY STORE.
     *
     */
    public function orderUpdate(request $request)
    {
        $this->OrderService->storeOrder($request);
        Session::flash('store', 'Order Updated Successfully');
        return redirect('order/order');
    }


}
