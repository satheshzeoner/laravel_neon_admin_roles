<?php

namespace App\Models\Enquiry;

use App\Models\Callhistory\Callhistory;
use App\Models\Order\Order;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'enquiry';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'customer_name',
        'phone_number',
        'service_id',
        'other_service_name',
        'enquiry_type',
        'description',
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = true;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    // const CREATED_AT = 'created';
    // const UPDATED_AT = 'updated';

    public function userList()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function callhistory()
    {
        return $this->hasOne(Callhistory::class, 'enquiry_id', 'id');
    }

    public function orderlist()
    {
        return $this->hasOne(Order::class, 'enquiry_id', 'id');
    }

}
