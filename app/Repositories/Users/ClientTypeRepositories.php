<?php

namespace App\Repositories\Users;

/* Models */

use App\Models\Client\ClientType;
use Illuminate\Support\Facades\Log;

class ClientTypeRepositories
{
    /* ===============================CREATE=============================== */

    /* Get Create Details */
    public function storeClientTypeDetails($clientDetails)
    {
        $clientDetailsResult = ClientType::create($clientDetails);
        Log::error('Client Details has Inserted :' . $clientDetailsResult->id);
        return $clientDetailsResult->id;
    }

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* Get Client Type Details */
    public function getClientTypeList()
    {
        return ClientType::all();
    }

    /* Get Specific Client Type Details */
    public function getSpecificClientTypeDetails($masterId)
    {
        return ClientType::whereId($masterId)->first();
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* Get Specific Client Type Details */
    public function updateClientType($masterId, $clientTypeDetails)
    {
        return ClientType::whereId($masterId)->update($clientTypeDetails);
    }

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* Get Specific Client Type Details */
    public function destroyClientType($masterId)
    {
        return ClientType::whereId($masterId)->delete();
    }

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
