<?php

namespace App\Repositories\Report;

/* Models */

use App\Models\Callhistory\Callhistory;
use App\Models\Enquiry\Enquiry;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ReportRepositories
{
    /* ===============================ENQUIRY QUERY================================= */

    /* Get Enquiry Details */
    public function getEnquiryReportList($searchParam)
    {

        $enquiryDetails = DB::table('enquiry');
        $enquiryDetails = $enquiryDetails->select('*');
        $enquiryDetails = $enquiryDetails->join('call_history', 'call_history.enquiry_id', '=', 'enquiry.id');
        $enquiryDetails = $enquiryDetails->join('users', 'users.id', '=', 'enquiry.user_id');

        if (!empty($searchParam['service_type'])) {
            $enquiryDetails = $enquiryDetails->where('enquiry.service_id', '=', $searchParam['service_type']);
        }

        if (!empty($searchParam['enquiry_type'])) {
            $enquiryDetails = $enquiryDetails->where('enquiry.enquiry_type', '=', $searchParam['enquiry_type']);
        }

        if (!empty($searchParam['text_search'])) {
            $enquiryDetails = $enquiryDetails->where('enquiry.customer_name', 'LIKE', "%{$searchParam['text_search']}%");
            $enquiryDetails = $enquiryDetails->orWhere('enquiry.phone_number', 'LIKE', "%{$searchParam['text_search']}%");
        }

        if (!empty($searchParam['from_date'])) {
            $enquiryDetails = $enquiryDetails->whereDate('enquiry.created_at', '>=', date('Y-m-d', strtotime($searchParam['from_date'])));
        }

        if (!empty($searchParam['to_date'])) {
            $enquiryDetails = $enquiryDetails->whereDate('enquiry.created_at', '<=', date('Y-m-d', strtotime($searchParam['to_date'])));
        }

        $enquiryDetails = $enquiryDetails->get();

        return $enquiryDetails;
    }


    /* ===============================ENQUIRY-END============================ */


    /* ===============================CALL QUERY================================= */

    /* Get Call Details */
    public function getCallReportList($searchParam)
    {

        $enquiryDetails = DB::table('call_history');
        $enquiryDetails = $enquiryDetails->select('*', DB::raw('call_history.description as call_description'));
//        $enquiryDetails = $enquiryDetails->select();
        $enquiryDetails = $enquiryDetails->join('enquiry', 'call_history.enquiry_id', '=', 'enquiry.id');
        $enquiryDetails = $enquiryDetails->join('users', 'users.id', '=', 'enquiry.user_id');

        if (!empty($searchParam['service_type'])) {
            $enquiryDetails = $enquiryDetails->where('enquiry.service_id', '=', $searchParam['service_type']);
        }

        if (!empty($searchParam['enquiry_type'])) {
            $enquiryDetails = $enquiryDetails->where('enquiry.enquiry_type', '=', $searchParam['enquiry_type']);
        }

        if (!empty($searchParam['call_status'])) {
            $enquiryDetails = $enquiryDetails->where('call_history.status_id', '=', $searchParam['call_status']);
        }

        if (!empty($searchParam['text_search'])) {
            $enquiryDetails = $enquiryDetails->where('enquiry.customer_name', 'LIKE', "%{$searchParam['text_search']}%");
            $enquiryDetails = $enquiryDetails->orWhere('enquiry.phone_number', 'LIKE', "%{$searchParam['text_search']}%");
        }

        if (!empty($searchParam['from_date'])) {
            $enquiryDetails = $enquiryDetails->whereDate('enquiry.created_at', '>=', date('Y-m-d', strtotime($searchParam['from_date'])));
        }

        if (!empty($searchParam['to_date'])) {
            $enquiryDetails = $enquiryDetails->whereDate('enquiry.created_at', '<=', date('Y-m-d', strtotime($searchParam['to_date'])));
        }

        $enquiryDetails = $enquiryDetails->get();

        return $enquiryDetails;
    }


    /* ===============================CALL-END============================ */

    /* ===============================ORDER QUERY================================= */

    /* Get Order Details */
    public function getOrderReportList($searchParam)
    {
        $enquiryDetails = DB::table('call_history');
        $enquiryDetails = $enquiryDetails->select('*');
        $enquiryDetails = $enquiryDetails->join('enquiry', 'call_history.enquiry_id', '=', 'enquiry.id');
        $enquiryDetails = $enquiryDetails->join('order', 'order.enquiry_id', '=', 'enquiry.id');
        $enquiryDetails = $enquiryDetails->join('users', 'users.id', '=', 'enquiry.user_id');

        if (!empty($searchParam['service_type'])) {
            $enquiryDetails = $enquiryDetails->where('enquiry.service_id', '=', $searchParam['service_type']);
        }

        if (!empty($searchParam['enquiry_type'])) {
            $enquiryDetails = $enquiryDetails->where('enquiry.enquiry_type', '=', $searchParam['enquiry_type']);
        }

        if (!empty($searchParam['track_status'])) {
            $enquiryDetails = $enquiryDetails->where('order.order_status', '=', $searchParam['track_status']);
        }

        if (!empty($searchParam['text_search'])) {
            $enquiryDetails = $enquiryDetails->where('enquiry.customer_name', 'LIKE', "%{$searchParam['text_search']}%");
            $enquiryDetails = $enquiryDetails->orWhere('enquiry.phone_number', 'LIKE', "%{$searchParam['text_search']}%");
        }

        if (!empty($searchParam['from_date'])) {
            $enquiryDetails = $enquiryDetails->whereDate('enquiry.created_at', '>=', date('Y-m-d', strtotime($searchParam['from_date'])));
        }

        if (!empty($searchParam['to_date'])) {
            $enquiryDetails = $enquiryDetails->whereDate('enquiry.created_at', '<=', date('Y-m-d', strtotime($searchParam['to_date'])));
        }

        $enquiryDetails = $enquiryDetails->get();

        return $enquiryDetails;
    }


    /* ===============================ORDER-END============================ */

}
