<?php

namespace App\Repositories\Callhistory;

/* Models */

use App\Models\Callhistory\Callhistory;
use App\Models\Enquiry\Enquiry;
use Illuminate\Support\Facades\Log;

class CallhistoryRepositories
{
    /* ===============================CREATE=============================== */

    /* Get Create Details */
    public function storeCallHistoryDetails($Details)
    {
        if (Callhistory::whereEnquiry_id($Details['enquiry_id'])->exists()) {
            Callhistory::whereEnquiry_id($Details['enquiry_id'])->update($Details);
            Log::info('CallHistory has Updated Enquiry Id :' . $Details['enquiry_id']);
            return $Details['enquiry_id'];
        } else {
            $detailsResult = Callhistory::Create($Details);
            Log::info('CallHistory Details has Inserted :' . $detailsResult->id);
            return $detailsResult->id;
        }

    }

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* Get Enquiry Details */
    public function getCallhistoryList()
    {
        return Enquiry::with(['userList', 'callhistory'])->orderBy('id', 'DESC')->get();
    }

    /* Get Specific Enquiry Details */
    public function getSpecificEnquiryDetails($masterId)
    {
        return Enquiry::whereId($masterId)->first();
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* Get Specific Enquiry Details */
    public function updateEnquiry($masterId, $details)
    {
        return Enquiry::whereId($masterId)->update($details);
    }

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* Get Specific Enquiry Details */
    public function destroyEnquiry($masterId)
    {
        return Enquiry::whereId($masterId)->delete();
    }

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
