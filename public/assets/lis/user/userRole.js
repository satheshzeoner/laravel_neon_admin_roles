/*
*
* Validation of Module Selection
*
* */

$(document).on('submit', '#form1', function (e) {
    var rolePermission = $('.select2 :selected').val();
    if (typeof rolePermission === "undefined") {
        $('.select2_error').css('color', '#cc2424');
        e.preventDefault();
    } else {
        $('.select2_error').css('color', '#ebebeb');
    }
})


/*
*
* Check Already Exist User Permission
*
* */

$(document).on('change', '.role_check', function (e) {

    $.ajax({
        type: "POST",
        url: base_url + "/usermanage/userrolecheck",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            "role_name": $(this).val(),
            "except_id": except_id,
        },
        cache: true,
        async: false,
        success: function (data) {
            if (data == 1) {
                $('.role_check').val('');
                $('.error-role').text('Role Name Already Exists').css('color', 'red');
            } else {
                $('.error-role').text('Role Name').css('color', '#303641');
            }
        }
    });

});

