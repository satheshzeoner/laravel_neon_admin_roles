<?php

return [
    'dates'            => [
        'date'   => range(1, 32),
        'months' => [
            array('key' => 1, 'value' => 'Jan'),
            array('key' => 2, 'value' => 'Feb'),
            array('key' => 3, 'value' => 'Mar'),
            array('key' => 4, 'value' => 'Apr'),
            array('key' => 5, 'value' => 'May'),
            array('key' => 6, 'value' => 'Jun'),
            array('key' => 7, 'value' => 'Jul'),
            array('key' => 8, 'value' => 'Aug'),
            array('key' => 9, 'value' => 'Sep'),
            array('key' => 10, 'value' => 'Oct'),
            array('key' => 11, 'value' => 'Nov'),
            array('key' => 12, 'value' => 'Dec')
        ],
        'years'  => range(1940, 2045),
    ],
    'user_nav_name'    => [
        'User_Management' => 1,
        'Enquiry'         => 2,
        'CallHistory'     => 3,
        'Order'           => 4,
        'Report'          => 5,
    ],
    'service'          => [
        1   => 'Panel board',
        2   => 'Laser Cutting',
        3   => 'Bending',
        4   => 'Fabrication',
        100 => 'Other',
    ],
    'enquiry_status'   => [
        1 => 'New Enquiry',
        2 => 'Existing Customer',
    ],
    'quotation_status' => [
        1 => 'Quotation Given',
        2 => 'Order-confirm',
        3 => 'Call Back',
        4 => 'Not Interested',
    ],
    'tracking_status'  => [
        1 => 'Order Taken',
        2 => 'Drawing',
        3 => 'Cutting',
        4 => 'Bending',
        5 => 'completed',
        6 => 'ready for delivery'
    ],
    'digits_of_count'  => [
        'count' => '%04d',
    ],
    'prefix_name'      => [
        'patient' => 'PAT',
    ],
];
