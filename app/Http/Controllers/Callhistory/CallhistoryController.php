<?php

namespace App\Http\Controllers\Callhistory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/* Services */

use App\Services\Callhistory\CallhistoryService;
use Illuminate\Support\Facades\Session;

class CallhistoryController extends Controller
{


    /**
     * @var CallhistoryService
     */
    private $CallhistoryService;

    public function __construct()
    {
        $this->CallhistoryService = new CallhistoryService();
    }

    /**
     * CALL HISTORY INDEX SCREEN.
     *
     */
    public function index()
    {
        $enquiry['callhistory'] = $this->CallhistoryService->getCallHistoryList();
        $enquiry['no']          = 0;
        return view('callhistory/callhistoryDisplay')->with($enquiry);
    }

    /**
     * CALL HISTORY STORE.
     *
     */
    public function callhistoryupdate(request $request)
    {
        $this->CallhistoryService->storeCallHistory($request);
        Session::flash('store', 'CallHistory Updated Successfully');
        return redirect('callhistory/callhistory');
    }


}
