<?php

namespace App\Services\Report;

use App\Models\Enquiry\Enquiry;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Exception;


/* Repository */

use App\Repositories\Report\ReportRepositories;

class ReportService
{

    public function __construct()
    {
        $this->ReportRepositories = new ReportRepositories();
    }

    # =============================================
    # =       ALL DETAILS ENQUIRY                 =
    # =============================================

    public function getEnquiryList($searchParam)
    {
        try {
            $details = $this->ReportRepositories->getEnquiryReportList($searchParam);
            return $details;
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =       ALL DETAILS CALL REPORT             =
    # =============================================

    public function getCallList($searchParam)
    {
        try {
            $details = $this->ReportRepositories->getCallReportList($searchParam);
            return $details;
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =       ALL DETAILS ORDER REPORT             =
    # =============================================

    public function getOrderList($searchParam)
    {
        try {
            $details = $this->ReportRepositories->getOrderReportList($searchParam);
            return $details;
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    public function doErrorFormat($responseResult)
    {
        Log::error($responseResult->getMessage());
        $result['status']  = false;
        $result['message'] = $responseResult->getMessage();
        return $result;
    }
}
