<?php

namespace App\Repositories\Order;

/* Models */

use App\Models\Order\Order;
use App\Models\Enquiry\Enquiry;
use Illuminate\Support\Facades\Log;

class OrderRepositories
{
    /* ===============================CREATE=============================== */

    /* Get Create Details */
    public function storeOrderDetails($Details)
    {
        if (Order::whereEnquiry_id($Details['enquiry_id'])->exists()) {
            Order::whereEnquiry_id($Details['enquiry_id'])->update($Details);
            Log::info('ORDER has Updated Enquiry Id :' . $Details['enquiry_id']);
            return $Details['enquiry_id'];
        } else {
            $detailsResult = Order::Create($Details);
            Log::info('ORDER Details has Inserted :' . $detailsResult->id);
            return $detailsResult->id;
        }

    }

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* Get Enquiry Details */
    public function getOrderList()
    {
        if (Enquiry::first() != null) {
            return Enquiry::with('userList')
                ->with('callhistory')
                ->with('orderlist')
                ->orderBy('id', 'DESC')
                ->get();
        } else {
            return [];
        }

    }

    /* Get Specific Enquiry Details */
    public function getSpecificEnquiryDetails($masterId)
    {
        return Enquiry::whereId($masterId)->first();
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* Get Specific Enquiry Details */
    public function updateEnquiry($masterId, $details)
    {
        return Enquiry::whereId($masterId)->update($details);
    }

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* Get Specific Enquiry Details */
    public function destroyEnquiry($masterId)
    {
        return Enquiry::whereId($masterId)->delete();
    }

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
