<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Users\UserPermission;
use App\Models\Users\UserPermissionModule;
use App\Models\Users\UserRole;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request)
    {
        /* Session Configuration Process */
        $this->sessionPermissionConfig($request);
        return redirect('home');
    }


    /*
     * Store Session Permission details
     * 
     * PUT Ex: $request->session()->put('user_permission', 'final-access-controll');
     * 
     * GET Ex: $request->session()->get('user_permission');
     * 
     * */
    public function sessionPermissionConfig($request)
    {
        $userId                = Auth::user()->role_id;
        $userRoleDetails       = UserRole::whereId($userId)->first();
        $userRoleSplit         = explode(',', $userRoleDetails->role_permission);
        $userPermissionDetails = UserPermission::whereIn('id', $userRoleSplit)->get()->toArray();

        if (isset($userPermissionDetails)) {
            $moduleId = [];
            foreach ($userPermissionDetails as $keyPer => $permissionValue) {
                $moduleSplitDetails = explode(',', $permissionValue['module_id']);
                foreach ($moduleSplitDetails as $modValue) {
                    $moduleId[] = $modValue;
                }
            }

            $userPermissionDetails = array_unique($moduleId);

            $finalModuleId = [];
            foreach ($userPermissionDetails as $key => $row) {
                $finalModuleId[$key] = $row;
            }

            $request->session()->put('user_permission', $finalModuleId);
        } else {
            $userPermissionDetails = UserPermissionModule::get()->toArray();
            $finalModuleId         = [];
            foreach ($userPermissionDetails as $userPermissionValue) {
                $finalModuleId[] = $userPermissionValue['id'];
            }
            $request->session()->put('user_permission', $finalModuleId);
        }

        Session::flash('welcome', 'Welcome to Pandiyan Industies PANEL');
        return true;

    }
}
