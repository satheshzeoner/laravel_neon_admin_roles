<?php

namespace App\Services\Users;

use App\Helpers\Helper;

use App\Models\Users\UserPermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception;


/* Repository */

use App\Repositories\Users\ClientTypeRepositories;

class ClienttypeService
{
    public function __construct()
    {
        $this->ClientTypeRepositories = new ClientTypeRepositories();
    }

    # =============================================
    # =       ALL DETAILS                         =
    # =============================================

    public function getClientTypeList()
    {
        try {
            $clientTypeDetails = collect($this->ClientTypeRepositories->getClientTypeList())->toArray();
            return $clientTypeDetails;
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =                 Add Details               =
    # =============================================

    public function storeClientType($request)
    {
        try {
            $clientType                            = $request->all();
            $clientTypeDetails['client_type_name'] = $clientType['client']['client_type_name'];
            return $this->ClientTypeRepositories->storeClientTypeDetails($clientTypeDetails);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =           Edit Details                    =
    # =============================================

    public function getClientTypeEditDetails($masterId)
    {
        try {
            return $this->ClientTypeRepositories->getSpecificClientTypeDetails($masterId);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =                  Update                  =
    # =============================================

    public function UpdateClientTypeDetails(request $request)
    {
        try {
            $clientType                            = $request->all();
            $masterId                              = $clientType['master_id'];
            $clientTypeDetails['client_type_name'] = $clientType['client']['client_type_name'];
            return $this->ClientTypeRepositories->updateClientType($masterId, $clientTypeDetails);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =             Destroy                       =
    # =============================================

    public function DestroyClientTypeDetails(request $request)
    {
        $postDetails = $request->all();
        try {
            return $this->ClientTypeRepositories->destroyClientType($postDetails['masterid']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    public function doErrorFormat($responseResult)
    {
        Log::error($responseResult->getMessage());
        $result['status']  = false;
        $result['message'] = $responseResult->getMessage();
        return $result;
    }
}
