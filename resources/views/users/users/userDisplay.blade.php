@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-6"><h3>Users</h3></div>
        <div class="col-md-6">
            <a href="{{ route('useradd') }}" class="btn btn-success" style="float: right">Add User</a>
        </div>
    </div>

    <br/>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            var $table4 = jQuery("#table-4");

            $table4.DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });
        });
    </script>


    <table class="table table-bordered datatable" id="table-4">
        <thead>
        <tr>
            <th>S.no</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>User Name</th>
            <th>Phone</th>
            <th>Roles</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($users as $row)
            <tr>
                <th>{{ ++$no }}</th>
                <td>{{ $row->fname }}</td>
                <td>{{ $row->lname }}</td>
                <td>{{ $row->email }}</td>
                <td>{{ $row->user_name }}</td>
                <td>{{ $row->phone }}</td>
                <td>
                    <div class="label label-primary">{{ $row->userRole->role_name }}</div>
                </td>
                <td class="text-center">
                    <a href="{{ url('usermanage/userupdate/'.$row->id) }}" class="btn btn-orange">
                        <i class="entypo-pencil"></i>
                    </a>

                    <button type="button" class="btn btn-danger" onclick="deleteUserRole({{ $row['id'] }})">
                        <i class="entypo-cancel"></i>
                    </button>

                    <a href="{{ url('usermanage/userview/'.$row->id) }}" class="btn btn-info">
                        <i class="entypo-info"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>



    {{-- Script For Delete --}}
    <script>
        function deleteUserRole(masterId) {
            jQuery('#modal-1').modal('show');
            $('.deleteid').val(masterId);
        }
    </script>

    {{-- DELETE MODEL--}}
    <div class="modal fade" id="modal-1">
        <div class="modal-dialog">
            <div class="modal-content" style="padding: 10px;">
                <form action="{{ route('userdestroy') }}" method="post">
                    @csrf
                    <div class="modal-body text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <div class="text-center" style="font-size: 15px;">Are you sure want to delete ?</div>
                        <br>
                        <input type="hidden" name="masterid" class="deleteid" value="">
                        <button type="submit" class="btn btn-danger">Yes</button>
                        <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
