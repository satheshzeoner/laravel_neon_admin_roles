<?php

use Illuminate\Database\Seeder;

class UserPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('users_permission')->get()->count() == 0) {
            DB::table('users_permission')->insert([
                'permission_name' => 'Admin Rights',
                'module_id'       => '1',
                'created_at'      => now()
            ]);
        } else {
            echo "Table is not empty";
        }
    }
}
