<?php

namespace App\Http\Controllers;

use App\Models\Callhistory\Callhistory;
use App\Models\Enquiry\Enquiry;
use App\Models\Order\Order;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;

/* Services */

use App\Services\Dashboard\DashboardService;

class HomeController extends Controller
{
    /**
     * @var DashboardService
     */
    private $DashboardService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->DashboardService = new DashboardService();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(request $request)
    {
        $dashboard['count'] = $this->todaysCount();
        $dashboard['no']    = 0;
        return view('home')->with($dashboard);
    }

    /* COUNT PROCESS */
    public function todaysCount()
    {
        $enquiryDetails  = Enquiry::whereDate('created_at', date('Y-m-d'))->get()->toArray();
        $orderDetails    = Order::whereDate('created_at', date('Y-m-d'))->get()->toArray();
        $callBackDetails = Callhistory::whereDate('created_at', date('Y-m-d'))->whereStatus_id('3')->get()->toArray();
        $user            = User::get()->toArray();

        $result['enquiry_count']   = count($enquiryDetails);
        $result['order_count']     = count($orderDetails);
        $result['call_back_count'] = count($callBackDetails);
        $result['user_count']      = count($user);

        return $result;
    }

}
