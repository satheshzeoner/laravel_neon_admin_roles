<?php

namespace App\Http\Controllers\Enquiry;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/* Services */

use App\Services\Enquiry\EnquiryService;
use Illuminate\Support\Facades\Session;

class EnquiryController extends Controller
{


    /**
     * @var EnquiryService
     */
    private $EnquiryService;

    public function __construct()
    {
        $this->EnquiryService = new EnquiryService();
    }

    /**
     * ENQUIRY INDEX SCREEN.
     *
     */
    public function index()
    {
        $enquiry['enquiry'] = $this->EnquiryService->getEnquiryList();
        $enquiry['no']      = 0;
        return view('enquiry/enquiryDisplay')->with($enquiry);
    }


    /**
     * ENQUIRY ADD SCREEN.
     *
     */
    public function enquiryAdd()
    {
        return view('enquiry/enquiryStore');
    }

    /**
     * ENQUIRY ADD STORE
     *
     */
    public function enquiryStore(request $request)
    {
        $this->EnquiryService->storeEnquiry($request);
        Session::flash('store', 'Enquiry Added Successfully');
        return redirect('enquiry/enquiry');
    }

    /**
     * ENQUIRY EDIT SCREEN
     *
     */
    public function enquiryEdit($masterId)
    {
        $enquiry['enquiry'] = $this->EnquiryService->getEnquiryEditDetails($masterId);
        return view('enquiry/enquiryUpdate')->with($enquiry);
    }

    /**
     * ENQUIRY UPDATE PROCESS
     *
     */
    public function enquiryUpdate(request $request)
    {
        $this->EnquiryService->UpdateEnquiryDetails($request);
        Session::flash('update', 'Enquiry Updated Successfully');
        return redirect('enquiry/enquiry');
    }

    /**
     * ENQUIRY ORDER STATUS UPDATION SCREEN
     *
     */
    public function enquiryOrderStatus($masterId)
    {
        $this->EnquiryService->enquiryOrderStatus($masterId);
        return view('enquiry/enquiry');
    }

    /**
     * ENQUIRY DELETE PROCESS
     *
     */
    public function enquiryDestroy(request $request)
    {
        $this->EnquiryService->DestroyEnquiryDetails($request);
        Session::flash('update', 'Enquiry Deleted Successfully');
        return redirect('enquiry/enquiry');
    }

}
