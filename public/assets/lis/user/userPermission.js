/*
*
* Validation of Module Selection
*
* */

$(document).on('submit', '#form1', function (e) {
    if ($("input:checkbox:checked").length > 0) {
    } else {
        alert('Please Select Any Permission Module List');
        e.preventDefault();
    }
});


/*
*
* Check Already Exist User Permission
*
* */

$(document).on('change', '.permission_check', function (e) {

    $.ajax({
        type: "POST",
        url: base_url + "/usermanage/userpermissioncheck",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            "permission_name": $(this).val(),
            "except_id": except_id,
        },
        cache: true,
        async: false,
        success: function (data) {
            if (data == 1) {
                $('.permission_check').val('');
                $('.error-permission').text('Permission Name Already Exists').css('color', 'red');
            } else {
                $('.error-permission').text('Permission Name').css('color', '#303641');
            }
        }
    });

});

