<table class="table table-bordered datatable" id="table-4">
    <thead>
    <tr>
        <th>S.no</th>
        <th>Attender</th>
        <th>Date</th>
        <th>Cust Name</th>
        <th>Phone</th>
        <th>Service</th>
        <th class="text-center">Enquiry-Type</th>
        {{--        <th class="text-center">Status</th>--}}
        {{--        <th class="text-center">Description</th>--}}
    </tr>
    </thead>
    <tbody>
    @foreach ($enquiry as $row)
        <tr>
            <td>{{ ++$no }}</td>
            <td>{{ $row->fname.'-'.$row->lname }}</td>
            <td>{{ date('d-m-Y H:i A',strtotime($row->created_at)) }}</td>
            <td>{{ $row->customer_name }}</td>
            <td>{{ $row->phone_number }}</td>
            <td>
                @if($row->service_id == 100)
                    {{ config('constants.service')[$row->service_id].'- ['.$row->other_service_name.']' }}
                @else
                    {{ config('constants.service')[$row->service_id] }}
                @endif
            </td>
            <td class="text-center">
                <div class="label @if($row->enquiry_type == 1) label-info @else label-primary  @endif ">{{ config('constants.enquiry_status')[$row->enquiry_type] }}</div>
            </td>
            {{--            <td class="text-center">--}}
            {{--                {{ config('constants.quotation_status')[$row->status_id] }}--}}
            {{--            </td>--}}
            {{--            <td>--}}
            {{--                {{ $row->description }}--}}
            {{--            </td>--}}
        </tr>
    @endforeach
    </tbody>
</table>


<script type="text/javascript">
    jQuery(document).ready(function ($) {
        var $table4 = jQuery("#table-4");

        $table4.DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ]
        });
    });
</script>