@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-sm-3 col-xs-6">

            <div class="tile-stats tile-red">
                <div class="icon"><i class="entypo-users"></i></div>
                <div class="num" data-start="0" data-end="{{ $count['enquiry_count'] }}" data-postfix=""
                     data-duration="1500"
                     data-delay="0">0
                </div>

                <h3>Today's Enquiry</h3>
                <p>No of Enquiry</p>
            </div>

        </div>

        <div class="col-sm-3 col-xs-6">

            <div class="tile-stats tile-green">
                <div class="icon"><i class="entypo-chart-bar"></i></div>
                <div class="num" data-start="0" data-end="{{ $count['order_count'] }}" data-postfix=""
                     data-duration="1500" data-delay="0">0
                </div>

                <h3>Today's Order</h3>
                <p>No of orders.</p>
            </div>

        </div>

        <div class="clear visible-xs"></div>

        <div class="col-sm-3 col-xs-6">

            <div class="tile-stats tile-aqua">
                <div class="icon"><i class="entypo-mail"></i></div>
                <div class="num" data-start="0" data-end="{{ $count['call_back_count'] }}" data-postfix="
                " data-duration="1500" data-delay="0">0
                </div>

                <h3>Today's CallBack</h3>
                <p>No of CallBack.</p>
            </div>

        </div>

        <div class="col-sm-3 col-xs-6">

            <div class="tile-stats tile-blue">
                <div class="icon"><i class="entypo-rss"></i></div>
                <div class="num" data-start="0" data-end="{{ $count['user_count'] }}" data-postfix=""
                     data-duration="1500" data-delay="0">0
                </div>

                <h3>Users</h3>
                <p>on our site right now.</p>
            </div>

        </div>
    </div>

    <br/>

    {{--    <div class="row">--}}
    {{--        <div class="col-lg-12">--}}
    {{--            <table class="table table-bordered datatable" id="table-4">--}}
    {{--                <thead>--}}
    {{--                <tr>--}}
    {{--                    <th>S.no</th>--}}
    {{--                    <th>Customer Name</th>--}}
    {{--                    <th>Phone Number</th>--}}
    {{--                    <th>Service</th>--}}
    {{--                    <th>Status</th>--}}

    {{--                </tr>--}}
    {{--                </thead>--}}
    {{--                <tbody>--}}
    {{--                @foreach ($call_back_lists as $row)--}}
    {{--                    <tr>--}}
    {{--                        <td>{{ ++$no }}</td>--}}
    {{--                        <td>{{ $row['customer_name'] }}</td>--}}
    {{--                        <td>{{ $row['phone_number'] }}</td>--}}
    {{--                        <td>--}}
    {{--                            @if($row['service_id'] == 100)--}}
    {{--                                {{ config('constants.service')[$row['service_id']].'- ['.$row['other_service_name'].']' }}--}}
    {{--                            @else--}}
    {{--                                {{ config('constants.service')[$row['service_id']] }}--}}
    {{--                            @endif--}}
    {{--                        </td>--}}
    {{--                        <td>--}}
    {{--                            <div class="label label-info">{{ config('constants.quotation_status')[$row['status']] }}</div>--}}
    {{--                        </td>--}}
    {{--                    </tr>--}}
    {{--                @endforeach--}}
    {{--                </tbody>--}}
    {{--            </table>--}}
    {{--        </div>--}}
    {{--    </div>--}}

    <script>
        $(document).ready(function () {
            toastr.options.timeOut = 1500; // 1.5s
            @if(Session::has('welcome'))
            toastr.info('{{Session::get('welcome')}}');
            @endif
        });

        jQuery(document).ready(function ($) {
            var $table4 = jQuery("#table-4");

            $table4.DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });
        });
    </script>


@endsection
